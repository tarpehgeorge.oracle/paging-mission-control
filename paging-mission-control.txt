
***Define the schema and load the input file , create a dataframe***

%python
from pyspark.sql.types import StructType
from pyspark.sql.types import StringType
from pyspark.sql.types import TimestampType
from pyspark.sql.types import StringType
from pyspark.sql.types import DecimalType
schema = StructType() \
      .add("timestamp",StringType(),True) \
      .add("satellite-id",StringType(),True) \
      .add("red-high-limit",StringType(),True) \
      .add("yellow-high-limit",StringType(),True) \
      .add("yellow-low-limit",StringType(),True) \
      .add("red-low-limit",StringType(),True) \
      .add("raw-value",DecimalType(),True) \
      .add("component",StringType(),True)
df = spark.read.format("csv") \
      .option("header", False) \
      .schema(schema) \
      .option("delimiter", "|") \
      .load("local/filename.txt")

***create a temporary table to use sqlcontext***

df.write.mode("append").format("delta").option("schema", "true").save("local/temp1.parquet")
spark.sql("CREATE TABLE IF NOT EXISTS table1 USING DELTA LOCATION 'local/temp1.parquet'")

***create a dataframe based on the condition as per statement where ` is used to skip the hypen used in columns***
%scala
val df1 =sqlContext.sql("""select `red-high-limit` as  `satelliteid`, `red-high-limit` as severity ,
CASE
    WHEN `component`='TSTAT'  THEN `severity` as `RED HIGH`
    WHEN `component`='BATT'  THEN  `severity` as `RED LOW`
    ELSE `severity`,
`component`,`timestamp`
from table1""")

***writing the dataframe to json ***
results = df1.toJSON()
results.collect()
or 
df1.coalesce(1).write.format ('json').save ('/path/file_name.json')
